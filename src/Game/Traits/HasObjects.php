<?php

namespace BinaryStudioAcademy\Game\Traits;

use BinaryStudioAcademy\Game\Contracts\Object;

trait HasObjects
{
    protected $objects = [];

    public function getObjects() : array
    {
        return $this->objects;
    }

    public function addObject(Object $object) : void
    {
        array_push($this->objects, $object);
    }

    public function removeObject($objectType)
    {
        foreach ($this->objects as $objectKey => $object) {
            if (get_class($object) === $objectType) {
                unset($this->objects[$objectKey]);
                return;
            }
        }
    }

    public function getCountObjects($objectType)
    {
        $result = 0;

        foreach ($this->objects as $object) {
            if (get_class($object) === $objectType) {
                $result += 1;
            }
        }

        return $result;
    }
}