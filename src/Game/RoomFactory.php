<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Rooms\Basement;
use BinaryStudioAcademy\Game\Rooms\Bedroom;
use BinaryStudioAcademy\Game\Rooms\Cabinet;
use BinaryStudioAcademy\Game\Rooms\Corridor;
use BinaryStudioAcademy\Game\Rooms\Hall;

class RoomFactory
{
    protected $rooms = [
        Hall::class,
        Basement::class,
        Bedroom::class,
        Cabinet::class,
        Corridor::class,
    ];

    public function setMode($mode)
    {
        foreach ($this->rooms as $room) {
            app()->{$mode}($room, function() use ($room) {
                return new $room;
            });
        }
    }
}