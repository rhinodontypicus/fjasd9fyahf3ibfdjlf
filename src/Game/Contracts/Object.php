<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Object
{
    public function getName() : string;
}