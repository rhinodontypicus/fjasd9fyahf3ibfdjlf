<?php

namespace BinaryStudioAcademy\Game\Contracts;

interface Room
{
    public function getName() : string;
    public function getDirections() : array;
    public function addDirection(string $direction) : void;
}
