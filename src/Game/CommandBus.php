<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Commands\Go;
use BinaryStudioAcademy\Game\Commands\Grab;
use BinaryStudioAcademy\Game\Commands\Help;
use BinaryStudioAcademy\Game\Commands\Observe;
use BinaryStudioAcademy\Game\Commands\Status;
use BinaryStudioAcademy\Game\Commands\Stop;
use BinaryStudioAcademy\Game\Commands\Where;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandBus
{
    /**
     * @var array
     */
    public static $commands = [
        Help::class,
        Stop::class,
        Where::class,
        Status::class,
        Observe::class,
        Go::class,
        Grab::class,
    ];

    /**
     * @param $input
     * @return bool
     */
    public function runCommand($input)
    {
        $cmd = $this->getCommand($input);

        if (! $command = $this->commandExists($cmd)) {
            return "Unknown command: '{$cmd['name']}'.";
        }

        $command = new $command($this, $cmd['arguments']);

        return $command->run();
    }

    /**
     * @param $input
     * @return array
     */
    private function getCommand($input)
    {
        $commandParts = explode(" ", $input);
        $commandName = $commandParts[0];
        array_shift($commandParts);

        return [
            'name' => $commandName,
            'arguments' => $commandParts,
        ];
    }

    /**
     * @param $command
     * @return bool
     */
    private function commandExists($command)
    {
        $result = false;

        foreach (self::$commands as $cmd) {
            if ($cmd::$name == $command['name']) {
                $result = $cmd;
            }
        }

        return $result;
    }
}