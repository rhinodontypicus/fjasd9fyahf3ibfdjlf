<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Objects\Coin;

class Game
{
    const COINS_TO_WIN = 5;
    private $roomFactory;
    private $commandBus;

    public function __construct()
    {
        $this->roomFactory = new RoomFactory();
        $this->commandBus = new CommandBus();

        app()->singleton(Player::class, function() {
            return new Player();
        });

        $this->roomFactory->setMode('singleton');
    }

    public function start(Reader $reader, Writer $writer): void
    {
        $writer->writeln("Welcome!");

        while (true) {
            $writer->write("Enter command: ");
            $input = trim($reader->read());
            $result = $this->commandBus->runCommand($input);

            if (is_array($result)) {
                $result = $this->commandBus->runCommand($result['run']);
            }

            $writer->writeln($result);

            if (self::isFinished()) {
                exit();
            }
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $input = trim($reader->read());
        $result = $this->commandBus->runCommand($input);

        if (is_array($result)) {
            $result = $this->commandBus->runCommand($result['run']);
        }

        $writer->write($result);
    }

    public static function isFinished()
    {
        return app(Player::class)->getCountObjects(Coin::class) >= Game::COINS_TO_WIN;
    }
}
