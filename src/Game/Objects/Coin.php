<?php

namespace BinaryStudioAcademy\Game\Objects;

use BinaryStudioAcademy\Game\Contracts\Object;

class Coin extends BaseObject implements Object
{
    protected $name = 'coin';
}