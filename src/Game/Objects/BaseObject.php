<?php

namespace BinaryStudioAcademy\Game\Objects;

use BinaryStudioAcademy\Game\Contracts\Object;

class BaseObject implements Object
{
    protected $name = 'object';

    public function getName() : string
    {
        return $this->name;
    }
}