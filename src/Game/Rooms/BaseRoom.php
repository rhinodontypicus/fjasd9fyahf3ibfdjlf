<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;
use BinaryStudioAcademy\Game\Traits\HasObjects;

class BaseRoom implements Room
{
    use HasObjects;

    protected $name = 'room';
    protected $defaultObjects = [];
    protected $directions = [];

    public function __construct()
    {
        foreach ($this->defaultObjects as $object) {
            $this->addObject(new $object);
        }
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getDirections() : array
    {
        return $this->directions;
    }

    public function addDirection(string $direction) : void
    {
        array_push($this->directions, $direction);
    }
}