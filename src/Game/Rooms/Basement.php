<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;
use BinaryStudioAcademy\Game\Objects\Coin;

class Basement extends BaseRoom implements Room
{
    protected $name = 'basement';
    protected $directions = [Cabinet::class, Hall::class];
    protected $defaultObjects = [Coin::class, Coin::class];
}