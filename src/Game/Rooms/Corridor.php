<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;

class Corridor extends BaseRoom implements Room
{
    protected $name = 'corridor';
    protected $directions = [Hall::class, Cabinet::class, Bedroom::class];
}