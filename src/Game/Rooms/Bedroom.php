<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;
use BinaryStudioAcademy\Game\Objects\Coin;

class Bedroom extends BaseRoom implements Room
{
    protected $name = 'bedroom';
    protected $directions = [Corridor::class];
    protected $defaultObjects = [Coin::class];
}