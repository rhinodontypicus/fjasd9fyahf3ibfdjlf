<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;
use BinaryStudioAcademy\Game\Objects\Coin;

class Hall extends BaseRoom implements Room
{
    protected $name = 'hall';
    protected $directions = [Basement::class, Corridor::class];
    protected $defaultObjects = [Coin::class];
}