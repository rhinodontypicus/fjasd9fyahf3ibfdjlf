<?php

namespace BinaryStudioAcademy\Game\Rooms;

use BinaryStudioAcademy\Game\Contracts\Room;
use BinaryStudioAcademy\Game\Objects\Coin;

class Cabinet extends BaseRoom implements Room
{
    protected $name = 'cabinet';
    protected $directions = [Corridor::class];
    protected $defaultObjects = [Coin::class];
}