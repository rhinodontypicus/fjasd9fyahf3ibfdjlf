<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Player;

class Go extends Command
{
    public static $name = 'go';
    public static $description = 'go to room';

    public function run()
    {
        $currentRoom = app(Player::class)->getCurrentRoom();
        $directions = $this->getAvailableDirections($currentRoom);

        if (!isset($this->arguments[0])) {
            return 'Enter valid room name.';
        }

        if (!in_array($this->arguments[0], $directions)) {
            return "Can not go to {$this->arguments[0]}.";
        }

        app(Player::class)->setCurrentRoom($this->getDirectionRoomClass($currentRoom));

        return ['run' => 'where'];
    }

    private function getAvailableDirections($currentRoom)
    {
        $result = [];

        foreach ($currentRoom->getDirections() as $direction) {
            array_push($result, (new $direction)->getName());
        }

        return $result;
    }

    private function getDirectionRoomClass($currentRoom)
    {
        foreach ($currentRoom->getDirections() as $direction) {
            if ((new $direction)->getName() == $this->arguments[0]) {
                return $direction;
            }
        }
    }
}