<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Player;

class Where extends Command
{
    public static $name = 'where';
    public static $description = 'show info about current position and possible directions';

    public function run()
    {
        $currentRoom = app(Player::class)->getCurrentRoom();

        return "You're at {$currentRoom->getName()}. You can go to: {$this->getAvailableDirections($currentRoom)}.";
    }

    private function getAvailableDirections($currentRoom)
    {
        $result = [];

        foreach ($currentRoom->getDirections() as $direction) {
            array_push($result, (new $direction)->getName());
        }

        return implode(', ', $result);
    }
}