<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\CommandBus;

class Help extends Command
{
    public static $name = 'help';
    public static $description = 'show available commands list';

    public function run()
    {
        $result = "\n";

        foreach (CommandBus::$commands as $command) {
            $result .= "{$command::$name} — {$command::$description}\n";
        }

        return $result;
    }
}