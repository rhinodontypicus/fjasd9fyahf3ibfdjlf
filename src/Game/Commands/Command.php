<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\CommandBus;

abstract class Command
{
    public static $name;
    public static $description;
    protected $arguments;
    protected $commandBus;

    /**
     * Command constructor.
     * @param CommandBus $commandBus
     * @param array $arguments
     */
    public function __construct($commandBus, array $arguments)
    {
        $this->arguments = $arguments;
        $this->commandBus = $commandBus;
    }

    /**
     * @return mixed
     */
    abstract public function run();
}