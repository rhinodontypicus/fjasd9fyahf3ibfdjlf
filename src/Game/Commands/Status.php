<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Objects\Coin;
use BinaryStudioAcademy\Game\Player;

class Status extends Command
{
    public static $name = 'status';
    public static $description = 'show info about current position and collected coins';

    public function run()
    {
        $currentRoom = app(Player::class)->getCurrentRoom();
        $player = app(Player::class);

        return "You're at {$currentRoom->getName()}. You have {$player->getCountObjects(Coin::class)} coins.";
    }
}