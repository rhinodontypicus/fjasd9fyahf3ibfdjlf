<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Objects\Coin;
use BinaryStudioAcademy\Game\Player;

class Observe extends Command
{
    public static $name = 'observe';
    public static $description = 'search coins in current room';

    public function run()
    {
        $currentRoom = app(Player::class)->getCurrentRoom();

        return "There {$currentRoom->getCountObjects(Coin::class)} coin(s) here.";
    }
}