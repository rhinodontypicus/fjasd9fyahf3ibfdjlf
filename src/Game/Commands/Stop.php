<?php

namespace BinaryStudioAcademy\Game\Commands;

class Stop extends Command
{
    public static $name = 'exit';
    public static $description = 'exit game';

    public function run()
    {
        exit();
    }
}