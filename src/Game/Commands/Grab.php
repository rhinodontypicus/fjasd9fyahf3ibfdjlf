<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Objects\Coin;
use BinaryStudioAcademy\Game\Player;

class Grab extends Command
{
    public static $name = 'grab';
    public static $description = 'grab 1 coin in room';

    public function run()
    {
        $currentRoom = app(Player::class)->getCurrentRoom();
        $countCoins = $currentRoom->getCountObjects(Coin::class);

        if ($countCoins <= 0) {
            return "There is no coins left here. Type 'where' to go to another location.";
        }

        $currentRoom->removeObject(Coin::class);
        app(Player::class)->addObject(new Coin);

        if (Game::isFinished()) {
            return "Good job. You've completed this quest. Bye!";
        }

        return 'Congrats! Coin has been added to inventory.';
    }
}