<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Rooms\Hall;
use BinaryStudioAcademy\Game\Traits\HasObjects;

class Player
{
    use HasObjects;

    private $currentRoom = Hall::class;

    public function setCurrentRoom($room)
    {
        $this->currentRoom = $room;
    }

    public function getCurrentRoom()
    {
        return app($this->currentRoom);
    }
}